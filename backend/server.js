import express from 'express';
import dotenv from 'dotenv';

import connectDB from './config/db.js';
import products from './apiRoutes/v1/products.js';

// Environment variables config
dotenv.config();

// Connect database 
connectDB();

const app = express();

// End points
app.get('/', (req, res) => {
  res.send('ecomms-shop-mvp_v1 API running ok..')
})


// Define Routes - Version 1
app.use('/api/v1/products', products);

const PORT = process.env.PORT || 5010;

app.listen(PORT, console.log(`ecomms-shop-mvp_v1 Server ok in ${process.env.NODE_ENV} mode @ port: ${PORT}`));