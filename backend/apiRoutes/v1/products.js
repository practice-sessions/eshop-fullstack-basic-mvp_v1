import express from 'express';
import asyncHandler from 'express-async-handler';
const apiRouter = express.Router();

// Get Product model
import Product from '../../models/productModel.js';


// @route   GET api/v1/products
// @desc    Get all products 
// @access  Public
apiRouter.get('/', asyncHandler(async (req, res) => {
  const products = await Product.find({});

  if(products) {
    res.json(products);
   } else {
    res.status(404)
    throw new Error('No Products found!');
   }

}));

// @route   GET api/v1/products/:id
// @desc    Get single product
// @access  Public
apiRouter.get('/:id', asyncHandler(async (req, res) => {
  const product = await Product.findById(req.params.id);

    if(product) {
     res.json(product);
    } else {
      res.status(404)
      throw new Error('Product not found!');
    }

}));


export default apiRouter;