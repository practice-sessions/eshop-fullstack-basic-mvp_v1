import bcrypt from 'bcryptjs';

const users = [
  {
    firstname: 'iAm',
    lastname: 'Admin',
    contactnumber: '0101',
    email: 'admin@text.com',
    isAdmin: true,
    password: bcrypt.hashSync('1234', 10)
  },
  {
    firstname: 'nowMista',
    lastname: 'Two',
    contactnumber: '1234',
    email: 'two@text.com',
    password: bcrypt.hashSync('1234', 10)
  },
  {
    firstname: 'andNow',
    lastname: 'Three',
    contactnumber: '2345',
    email: 'three@text.com',
    password: bcrypt.hashSync('1234', 10)
  }
];

export default users;