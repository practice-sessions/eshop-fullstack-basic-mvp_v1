import React, { useState, useEffect } from 'react';

import Product from '../components/Product';
import axios from 'axios';

const HomeScreen = () => {

  const [products, setProducts] = useState([]);

  useEffect(() => {
    const bringProducts = async () => {
      const { data } = await axios.get('/api/v1/products')

      setProducts(data);
    }

    bringProducts();
  }, [])

  return (
    <>
      {/* <div className='container'> */}

        <div className='row'>
          <div className='col s12 m4 l8'>
            <h3>Our Best Products</h3>
          </div>  
        </div>

        <div className='row'>  
          <div className='container'>
            {products.map((product) => (
              <div key={product._id} className='grid-products col s12 m6 l4 xl3'>
  
                <Product product={product} />
                
              </div>
            ))}
          </div>
        </div>
        
      {/* </div> */}
      
    </>
  )
}

export default HomeScreen;
