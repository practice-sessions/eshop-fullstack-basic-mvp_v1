import React, { useEffect } from 'react';
import { BrowserRouter as Router,  Route } from 'react-router-dom';

import 'materialize-css/dist/css/materialize.min.css';
import M from 'materialize-css/dist/js/materialize.min.js';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import HomeScreen from '../src/screens/HomeScreen';
import ProductScreen from '../src/screens/ProductScreen';

const App = () => {

  useEffect(() => {
    // Init Materialize JS
    M.AutoInit();

    // Sidenav
    M.Sidenav.init();
  });

  return (
    <Router>
      <Navbar />
      
      <main>
      <div className='container'>
        <Route path='/' component={HomeScreen} exact />
        <Route path='/product/:id' component={ProductScreen} />
      </div>  
      </main>
      <Footer />
    </Router>
  );
}

export default App;
